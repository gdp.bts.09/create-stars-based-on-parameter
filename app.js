// function untuk generate star, 
function generateStar(starType, size) {
    let generate = "";
    const printed = '*';
    if (size < 3) {
        size = 3;
    }
    switch(getStarType()){
        case 1:
            starOne();
            break;
        case 2:
            starTwo();
            break;
        case 3:
            starThree();
            break;
        case 4:
            starFour();
            break;
        case 5: 
            starFive();
            break;
        case 6:
            starSix();
            break;
        case 7:
            starSeven();
            break;
        case 8:
            starEight();
            break;
        case 9:
            starNine();
            break;
    }

    function getStarType(){
        return starType;
    }

    function starOne() {
        for (let i = 0; i < size; i++) {
            for (let j = 0; j < size - i; j++) {
                generate += printed;
            }
            generate += '\n'
        }
        console.log(generate);
    }

    function starTwo() {
        for (let i = 0; i < size; i++) {
            for (let j = 0; j <= i; j++) {
                generate += printed;
            }
            generate += "\n";
        }
        console.log(generate)
    }

    function starThree() {
        for (let i = 0; i < size; i++) {
            for (let j = 0; j <= i; j++) {
                generate += printed;
            }
            generate += "\n";
        }
        for (let i = 0; i < size; i++) {
            for (let j = 0; j < size - i; j++) {
                generate += '*';
            }
            generate += "\n";
        }
        console.log(generate)
    }

    function starFour() {
        for (let i = 0; i < size; i++) {
            if (i % 2 == 0) {
                for (let j = 0; j < size; j++) {
                    if (j % 2 == 0) {
                        generate += printed;
                    } else {
                        generate += ' ';
                    }
                }
                generate += "\n";
            } else {
                for (let j = 0; j < size; j++) {
                    if (j % 2 != 0) {
                        generate += printed;
                    } else {
                        generate += ' ';
                    }
                }
                generate += "\n";
            }
        }
        console.log(generate);
    }

    function starFive() {
        for (let i = size; i >= 0; i--) {
            for (let j = 0; j < size - i; j++) {
                generate += " ";
            }
            for (let k = 0; k < 2 * i - 1; k++) {
                generate += printed;
            }
            generate += "\n";
        }
        console.log(generate);
    }

    function starSix() {
        for (let i = 0; i <= size; i++) {
            for (let j = 0; j < size - i; j++) {
                generate += " ";
            }
            for (let k = 0; k < 2 * i - 1; k++) {
                generate += printed;
            }
            generate += "\n";
        }
        console.log(generate);
    }

    function starSeven() {
        for (let i = 5; i >= 0; i--) {
            if (i == 0) {
                break;
            }
            for (let j = 0; j < 5 - i; j++) {
                generate += " ";
            }
            for (let k = 0; k < 2 * i - 1; k++) {
                generate += printed;
            }
            generate += "\n";
        }
        for (let i = 0; i <= 5; i++) {
            for (let j = 0; j < 5 - i; j++) {
                generate += " ";
            }
            for (let k = 0; k < 2 * i - 1; k++) {
                generate += printed;
            }
            generate += "\n";
        }
        console.log(generate);
    }

    function starEight() {
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (i == 0 || i == 4) {
                    generate += printed;
                } else {
                    if (j == 0 || j == 4) {
                        generate += printed;
                    } else {
                        generate += ' ';
                    }
                }
            }
            generate += "\n";
        }
        console.log(generate);
    }

    function starNine() {
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5 - i; j++) {
                generate += " ";
            }
            for (let k = 0; k < 2 * i - 1; k++) {
                if (i === 1 || i === 4) {
                    generate += printed
                }
                else {
                    if (k === 0 || k === 2 * i - 2) {
                        generate += printed
                    }
                    else {
                        generate += " ";
                    }
                }
            }
            generate += "\n";
        }
        console.log(generate);
    }

}

generateStar(9, 5);